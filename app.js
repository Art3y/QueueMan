const express = require('express')
const app = express()
var bp = require('body-parser'),
	mongoose = require('mongoose'),
	passport = require("passport"),
	localStratergy = require("passport-local"),
	methodOverride = require('method-override'),
	User = require('./models/user'),
	Event = require('./models/event')

var QRCode = require('qrcode'),
	urlid

var bcrypt = require('bcrypt'),
	saltRounds = 10

/*===========
Configuration
===========*/
mongoose.connect("mongodb://localhost/JamServers")
app.use(bp.urlencoded({extended: true}));
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
app.set("view engine", "ejs");

/*====================
Passport configuration
====================*/
app.use(require('express-session')({
	secret: "First come first served services",
	resave: false,
	saveUninitialized: false
}))
app.use(require('flash')())
app.use(passport.initialize())
app.use(passport.session())
passport.use(new localStratergy({
	passReqToCallback: true,
	usernameField: "uName",
	passwordField: "uPassword"
	},
	function(req, username, password, done) {
		User.findOne({username: username}, function(err, user) {
			if(err) {
				return done(err)
			}
			if (!user) {
				console.log('no user');
				return done(null, false, { message: 'Incorrect username.' });
			}
			if (!user.validPassword(password)) {
				console.log('no password');
        		return done(null, false, { message: 'Incorrect password.' });
      		}
      		return done(null, user);
		})
	}
))
passport.serializeUser(function(user, done) {
  	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  	User.findById(id, function(err, user) {
    	done(err, user);
  	});
});

/*==========
Landing page
==========*/
app.get("/", function(req, res) {
	res.render('home')
})

/*==============
Add Participants
==============*/

app.get('/event/:id/addParticipants', function(req, res) {
	User.find({}, function(err, userArray) {
		res.render('database/add', {names: userArray, eid: req.params.id})
	})
})

app.post('/event/:id/addParticipants', function(req, res) {
	var newParti = req.body.participant
	var newID = Date.now()
	var newLink
	var raw = [
		{data: newID, mode: 'numeric'}
	]
	QRCode.toDataURL(raw, function (err, url) {
		Event.findById(req.params.id, function(err, foundEvent) {
			if(err) {
				console.log('Unable to add Participants')
				console.log(err)
			}
			//console.log(foundEvent);
			User.find({username: newParti}, function(err, foundUser) {
				if(err) {
					console.log('Unable to add Participants')
					console.log(err)
				}
				foundEvent.activeParitcipats = foundEvent.activeParitcipats + 1
				foundEvent.eventParticipants.id.push(foundUser[0]._id)
				foundEvent.eventParticipants.qrID.push(newID)
				foundEvent.save()
				foundUser[0].participatedEventCount = foundUser[0].participatedEventCount + 1
				foundUser[0].eventsParticipated.id.push(req.params.id)
				foundUser[0].eventsParticipated.qrLink.push(url)			
				foundUser[0].save()
			})
			res.redirect('/event/' + req.params.id)	
		})
	})
})

/*=====================
Event registration form
======================*/
app.get("/eregister", function(req, res) {
	res.render("database/eregister")
})

app.post("/eregister", function(req, res) {
	var start = req.body.eStartTime
	var end = req.body.eEndTime
	var date = req.body.eDate
	var Stsecond = 60 * ((parseInt(start[0] * 10) + parseInt(start[1])) * 60 + parseInt(start[0] * 10) + parseInt(start[1]))
	var Ensecond = 60 * ((parseInt(start[0] * 10) + parseInt(start[1])) * 60 + parseInt(start[0] * 10) + parseInt(start[1]))
	var parsedDate = new Date(Date.parse(date))
	Stsecond = (parsedDate.getTime() + Stsecond)
	Ensecond = (parsedDate.getTime() + Ensecond)

	var newEvent = {eventName: req.body.eName, eventDescription: req.body.eDescription, eventCapacity: req.body.eCapacity, activeParitcipats: 0,
		eventStart_time: Stsecond, eventEnd_time: Ensecond, eventDate: req.body.eDate, activeAdmins: 1}
	Event.create(newEvent, function(err, newlyCreated) {
		if(err) {
			console.log('eregister error');
			console.log(err);
		}
		//Add as the admin to event module
		newlyCreated.eventAdmins.push(req.user)
		newlyCreated.save()
		//Add as admin to user module
		req.user.eventsAdmin.push(newlyCreated)
		req.user.adminEventCount = req.user.adminEventCount + 1
		req.user.save()
		res.redirect('/event/' + newlyCreated._id)
	})
})

app.get("/event/:id", function(req, res) {
	Event.findById(req.params.id, function(err, foundEvent) {
		if(err) {
			console.log('Finding the hosting event')
			console.log(err)
		}
		res.render('screen/myEvent', {event: foundEvent})
	})
})

// app.get("/participants", function(req, res) {
// 	console.log(req.query);
// 	if(req.query.search) {
// 		const regex = new RegExp(escapeRegex(req.query.search), 'gi');
// 		User.findOne({username: regex}, function(err, results) {
// 			if(err) {
// 				console.log('Participants Query')
// 				console.log(err)
// 			}
// 			res.redirect('/user/' + results._id)
// 		})
// 	}
// })

/*====================
User registration form
====================*/
app.get("/uregister", function(req, res) {
	res.render("database/uregister")
})

app.post("/uregister", function(req, res) {
	var pass = req.body.uPassword
	bcrypt.hash(pass, saltRounds, function(err, hash) {
		var newUser = {fullName: req.body.ufname, username: req.body.uName, age: req.body.uAge, email: req.body.uEmail,
			password: hash, adminEventCount: 0, participatedEventCount: 0};
		User.create(newUser, function(err, newlyCreated) {
			if(err) {
				console.log('uregister error');
				console.log(err);
			}
			res.redirect('/user/' + newlyCreated._id)
		})
	})
})

app.get("/ulogin", function(req, res) {
	res.render("database/uregister")
})

app.post('/ulogin', passport.authenticate('local', { successRedirect: '' , failureRedirect: '/' , failureFlash: true}),
	function(req, res) {
	User.findOne({username: req.body.uName}, function(err, foundUser) {
		if(err) {
			console.log('Unable to login')
			console.log(err)
		}
		res.redirect('/user/' + foundUser._id)
	})
})

app.get("/user/:uid", function(req, res) {
	User.findById(req.params.uid, function(err, foundUser) {
		if(err) {
			console.log('Unable to find the user')
			console.log(err)
		}
		Event.findById(foundUser.eventsParticipated.id, function(err, foundEvents) {
			if(err) {
				console.log(err)
			}
			console.log('array' + foundEvents)
		})
		res.render('screen/myProfile', {user: foundUser})
	})
})

app.get("/ulogout", function(req, res) {
	req.logout()
	res.redirect("/")
})

function isLoggedIn(req, res, next) {
	if(req.isAuthenticated()) {
		return next()
	}
	res.redirect("home")
}

// function escapeRegex(text) {
//     return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
// }

/*=====
Hosting
=====*/
app.listen(3000, function() {
	console.log('The queue starts here');
})