var mongoose = require('mongoose')

var eventSchema = new mongoose.Schema({
	eventName: String,
	eventDay: Date,
	eventStart_time: Number,
	eventEnd_time: Number,
	eventCapacity: Number,
	eventDescription: String,
	activeAdmins: Number,
	activeParitcipats: Number,
	eventAdmins: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: "User"
		}
	],
	eventParticipants: {
		id: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: "User"
			}
		],
		qrID: [Number]
	}
	//eventVenue: 
})
module.exports = mongoose.model('Event', eventSchema)