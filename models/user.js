var mongoose = require('mongoose')
var passportLocalMongoose = require('passport-local-mongoose')
var bcrypt = require('bcrypt');

var userSchema = new mongoose.Schema({
	fullName: String,
	username: String,
	age: Number,
	email: String,
	password: String,
	adminEventCount: Number,
	participatedEventCount: Number,
	eventsAdmin: [
		{
			type: mongoose.Schema.Types.ObjectId,
			ref: "Event"
		}
	],
	eventsParticipated: {
		id: [
			{
				type: mongoose.Schema.Types.ObjectId,
				ref: "Event"
			}
		],
		qrLink: [String]
	}
});

userSchema.methods.validPassword = function(pwd) {
    return bcrypt.compareSync(pwd, this.password);
};

userSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('User', userSchema)